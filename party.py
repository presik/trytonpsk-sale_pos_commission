# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date
from trytond.pool import Pool
from trytond.report import Report


class AgentDunningReport(Report):
    'Agent Dunning  Report'
    __name__ = 'sale_pos_commission.agent_dunning'

    # @classmethod
    # def get_context(cls, records, header, data):
    #     report_context = super().get_context(records, header, data)
    #     pool = Pool()
    #     Company = pool.get('company.company')
    #     Invoice = pool.get('account.invoice')
    #     _records = []
    #     now = date.today()
    #     for party in records:
    #         invoices = Invoice.search([
    #             ('agent.party', '=', party.id),
    #             ('state', 'in', ['posted', 'validated']),
    #             ('type', '=', 'out'),
    #             ], order=[('invoice_date', 'ASC')])

    #         posted_invoices = []
    #         sum_invoices = []
    #         sum_amount_to_pay = []

    #         for invoice in invoices:
    #             sum_invoices.append(invoice.total_amount)
    #             sum_amount_to_pay.append(invoice.amount_to_pay)
    #             aged = ''
    #             if invoice.invoice_date:
    #                 aged = (now - invoice.invoice_date).days
    #             setattr(invoice, 'aged', aged)
    #             posted_invoices.append(invoice)

    #         setattr(party, 'invoices', posted_invoices)
    #         setattr(party, 'sum_invoices', sum(sum_invoices))
    #         setattr(party, 'sum_amount_to_pay', sum(sum_amount_to_pay))
    #         _records.append(party)

    #     report_context['records'] = _records
    #     report_context['company'] = Company(1)
    #     return report_context

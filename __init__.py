# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import configuration
from . import statement
from . import sale
from . import party


def register():
    Pool.register(
        # configuration.Configuration,
        # statement.StatementLine,
        # sale.Sale,
        module='sale_pos_commission', type_='model')
    Pool.register(
        # party.AgentDunningReport,
        module='sale_pos_commission', type_='report')
    Pool.register(
        # sale.SaleForceDraft,
        module='sale_pos_commission', type_='wizard')

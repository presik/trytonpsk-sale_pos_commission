# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta


class ExpiredPortfolioReport(metaclass=PoolMeta):
    __name__ = 'invoice_report.expired_portfolio_report'

    # @classmethod
    # def get_notes(cls, invoice):
    #     if invoice.agent:
    #         return invoice.agent.rec_name
